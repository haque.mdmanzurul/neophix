<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Neophix');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XQ}ylOP}<:~ED.NXJsNJdO&d>oO>D.HQs~LKQ$:Gl42]z#nE6UBUR#U_rgC.W$u;');
define('SECURE_AUTH_KEY',  '`va.0a9cD55&OZ8*vyK8?|Ht,k`V]~*$RL:(M>yCbU^ne ,mD}-P>re !4e qA|]');
define('LOGGED_IN_KEY',    'sO_WgilCUBKkX^N._4)?E7K0e,FJ*b7M!XRO)Q1%g?Oi9{fC*Vs~+MB/*6w#V>?=');
define('NONCE_KEY',        '+ToJXcJ@:%:.3Y?E*k r-irLz0CV?_GSBik[L>o1V8el~D[j67[2@>p[Z(Q`O~=N');
define('AUTH_SALT',        'XQnj$o|,9}:e`u~1/iVFdZ;[9WpjeqBS(.j9TbM1NO:k7@|E7LH1tiSX<0NUz1j?');
define('SECURE_AUTH_SALT', '3Ldu[aBd1`ixpe`2!JJji3eth1E%[=>WK;; !.T6YyrBPaV`(X5*]jUD=%rIUY]S');
define('LOGGED_IN_SALT',   'i;GAqxEC;^/u<oUpX(>,5j~CiQigup}(>7g7Ob7=$1@r6`{co abU/;Qgfpn,%L(');
define('NONCE_SALT',       'ugrC 2H{=4t9c(QnLJ5&$`v#hm43-y)2X}~Uh:d;|/07T3F=C5:f~9m3wd3*>VKl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
