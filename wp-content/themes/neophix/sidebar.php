<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="pro-box-con">
  <?php $args = array( 'post_type' => 'home-block', 'posts_per_page' => 10 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
                     echo '<div class="pro-box">';			
                        the_post_thumbnail(   ); 
                             echo '<div class="pro-det">';
                            	 echo '<h1>'; the_title();echo '</h1>';
								 ?> 
                                	 <a class="view" href="<?php the_field ('url'); ?>"><?php the_field ('button_label'); ?></a>
                                     
                                  <?php
								  
                             echo '</div>';
						 echo '</div>';
                    endwhile; ?>
</div>

 