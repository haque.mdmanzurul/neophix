<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	  <section class="blue-section">
       <div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	<?php dynamic_sidebar( 'sidebar-testmonial' ); ?>
    		    </div>
     		</div>
    	 </div>
     </section>
     
     
     <footer>
     	<div class="container">
        	<div class="row">
            	<div class="col-lg-7 col-sm-7 col-md-7 foot-lt">
                	<?php wp_nav_menu( array( 'theme_location' => 'footerone', 'menu_class' => 'main-nav', 'menu_id' => 'footerone-menu' ) ); ?> 
                    <?php wp_nav_menu( array( 'theme_location' => 'footertwo', 'menu_class' => 'product-nav', 'menu_id' => 'footertwo-menu' ) ); ?> 
                    <?php wp_nav_menu( array( 'theme_location' => 'footerthree', 'menu_class' => 'app-nav', 'menu_id' => 'footerthree-menu' ) ); ?> 
                    <?php wp_nav_menu( array( 'theme_location' => 'footerfour', 'menu_class' => 'last-nav', 'menu_id' => 'footerfour-menu' ) ); ?> 
                        
                </div>
                <div class="col-lg-5 col-sm-5 col-md-5 foot-rt">
               	  <?php $args = array( 'post_type' => 'sociallinks', 'posts_per_page' => 1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
                     echo ' <ul class="foot-social-link">';
					 	echo '<li>';
                       ?> 
                        <a><span> SOCIAL MEDIA</span></a>
                        <?php
						echo '</li>';			
                          echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('facebook_link'); ?>"><img src="<?php echo the_field ('facebook-icon'); ?>"></a>
                        <?php
						echo '</li>';
						
						 echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('twitter_link'); ?>"><img src="<?php echo the_field ('twitter_icon'); ?>"></a>
                        <?php
						echo '</li>';
						
						 echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('google_link'); ?>"> <img src="<?php echo  the_field ('google_icon'); ?>"></a>
                        <?php
						echo '</li>';
						
					echo '</ul>';
                    endwhile; ?>
                    
                    
                    <div class="foot-address">
                    	<a href="<?php bloginfo( 'url' ); ?>"> <img src="<?php echo get_option_tree( 'footer_logo' ); ?>"></a>
                        
                      <?php dynamic_sidebar( 'sidebar-footeraddress' ); ?> 
                    </div>
              </div>
              <div class="clearfix"></div>
              
              <div class="col-lg-5 col-sm-5 col-md-5 footlogo">
              	<ul>
                	<?php if( get_option_tree( 'footer_left_logo_1_link' )!= "" ) {  ?>
                    <li><a href="<?php echo get_option_tree( 'footer_left_logo_1_link' ); ?>"><img src="<?php echo get_option_tree( 'footer_left_logo_1' ); ?>"></a></li>
                    <?php } ?>
                    
                    <?php if( get_option_tree( 'footer_left_logo_2_link' )!= "" ) {  ?>
                    <li><a href="<?php echo get_option_tree( 'footer_left_logo_2_link' ); ?>"><img src="<?php echo get_option_tree( 'footer_left_logo_2' ); ?>"></a></li>
                    <?php } ?>
                    
                    <?php if( get_option_tree( 'footer_left_logo_3_link' )!= "" ) {  ?>
                    <li><a href="<?php echo get_option_tree( 'footer_left_logo_3_link' ); ?>"><img src="<?php echo get_option_tree( 'footer_left_logo_3' ); ?>"></a></li>
                    <?php } ?>
					
					
                </ul>
              </div>
              <div class="col-lg-7 col-sm-7 col-md-7 foot-rt-copy">
              	 
                <p>&copy; <?php echo get_option_tree( 'copyright_text' ); ?> </u></p>
              </div>
            </div>
        </div>
     </footer>


<!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie10-viewport-bug-workaround.js"></script>
     <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.slicknav.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#primary-menu').slicknav();
		});
    </script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cbpFWSlider.min.js"></script>
		<script>
			$( function() {
				/*
				- how to call the plugin:
				$( selector ).cbpFWSlider( [options] );
				- options:
				{
					// default transition speed (ms)
					speed : 500,
					// default transition easing
					easing : 'ease'
				}
				- destroy:
				$( selector ).cbpFWSlider( 'destroy' );
				*/
				$('document').ready(function(){
                // init slider
                $('#cbp-fwslider').cbpFWSlider();

               /**
                     Set a 3 seconds interval
                     if next button is visible (so is not the last slide)  click next button
                     else it finds first dot and click it to start from the 1st slide
                **/
                setInterval( function(){
                    if($('.cbp-fwnext').is(":visible"))
                        {
                            $('.cbp-fwnext').click();   


                }
                else{
                        $('.cbp-fwdots').find('span').click();
                    }
            } ,10000 );
        });

				$( '#cbp-fwslider' ).cbpFWSlider();

			} );
		</script> 
 
<?php wp_footer(); ?>

</body>
</html>
