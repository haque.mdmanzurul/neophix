<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
    
    
	<?php wp_head(); ?>
       <!-- Bootstrap core CSS -->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/layout.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/fonts.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/slicknav.css">
    
</head>

<body <?php body_class(); ?>>
<div class="header-wrap">
     <header>
         <div class="container">
        	<div class="row">
            	<div class="col-lg-3 col-md-3 col-sm-3 logo">
                	<a href="<?php bloginfo( 'url' ); ?>"> <img src="<?php echo get_option_tree( 'logo' ); ?>"></a>
                </div>
                <div class="desktop-hide responsive-nav">
                	 <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'menu', 'menu_id' => 'primary-menu' ) ); ?>  

                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 hide-mobile">
                <div class="top-links">
                	
                     
                    
                     <?php $args = array( 'post_type' => 'sociallinks', 'posts_per_page' => 1 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
                     echo ' <ul>';			
                          echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('facebook_link'); ?>"><img src="<?php echo the_field ('facebook-icon'); ?>"></a>
                        <?php
						echo '</li>';
						
						 echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('twitter_link'); ?>"><img src="<?php echo the_field ('twitter_icon'); ?>"></a>
                        <?php
						echo '</li>';
						
						 echo '<li>';
                       ?> 
                        <a class="view" href="<?php the_field ('google_link'); ?>"> <img src="<?php echo  the_field ('google_icon'); ?>"></a>
                        <?php
						echo '</li>';
						
					echo '</ul>';
                    endwhile; ?>
                    <?php dynamic_sidebar( 'sidebar-topsocial' ); ?>
                    
                </div>
                <div class="menu-con">
                	 <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => ' ', 'menu_id' => 'primary-menu' ) ); ?> 
                </div>
                </div>
                <div class="clearfix"></div>
            
                <div class="col-lg-12  ">
                	 
                    <div class="line">
                    <div class="pro-box-con">
                    
                      
                     
                     
                     <?php $args = array( 'post_type' => 'home-block', 'posts_per_page' => 10 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
                     echo '<div class="pro-box">';			
                        the_post_thumbnail(   ); 
                             echo '<div class="pro-det">';
                            	 echo '<h1>'; the_title();echo '</h1>';
								 ?> 
                                	 <a class="view" href="<?php the_field ('url'); ?>"><?php the_field ('button_label'); ?></a>
                                     
                                  <?php
								  
                             echo '</div>';
						 echo '</div>';
                    endwhile; ?>
                    
						 
						 
						 
						
                          
                    </div>
                  </div> 
                </div>
          </div>
            
        </div>
        
     </header>
     <div id="cbp-fwslider" class="cbp-fwslider">
          		
				<ul>
				<?php $args = array( 'post_type' => 'slider-post', 'posts_per_page' => 10 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
      			?>
				 <?php
					  echo '<li>';
					 ?>  
					 
                    <?php
                            echo '<a href="#"> ';
                    the_post_thumbnail(   ); 
                    
                    echo '</a>';
					
					?>
                    
					
					   <?php
                            echo '<div class="cap">';
                            echo '<div class="container">';
                            echo ' <div class="row"> ';
                            echo '<div class="col-lg-6 col-md-6 col-sm-6 top-title">';
                        ?>
                         <?php
					  	  echo '<h2>'; the_title();echo '</h2>';
						  echo '<p>'; the_content();echo '</p>';
                         ?>
                          
                        <?php
					  	  echo '</div>'
						?>
              	   <?php
					  	 echo '<div class="col-lg-6 col-md-6 col-sm-6 hide-mobile">';
                         echo '<div class="slider-con">';
				   ?>
                         <img src="<?php the_field ('caption_image'); ?>"> 
                   <?php
					  	 echo '</div>';
                         echo '</div>';
                 		 echo '</div>';
                         echo '</div>';
                         echo '</div>';
					   
				  ?>
                  
                  <?php
					echo '</li>';
					endwhile;
				  ?>
                    
                    
                    
                    
				 
					 
				</ul>
			</div>
  </div> 