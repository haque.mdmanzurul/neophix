<?php
/**
 * Template Name: Product single Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('page'); ?>
<section class="gray-section about-page">
     	<div class="container">
        	<div class="row">
             			
            
            	 <div class="page-lt">
                       <?php if (have_posts()) : while (have_posts()) : the_post();?>
                             <?php the_content( ); ?>
                 		<?php endwhile; endif; ?>
                 	
              </div>
                 <div class="page-rt">
                 <?php get_sidebar(); ?>
                 </div>
          </div>
        </div>
     </section>
<?php get_footer(); ?>
