<?php
/**
 * Template Name: Product Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('page'); ?>
<section class="gray-section about-page">
     	<div class="container">
        	<div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12">
                 	<div class="pro-box-con productbox">
                      
                    
                    
                     
                    	 
                    
                      
                   <?php
				  
					 $mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );
					 
					 foreach( $mypages  as $page ) {
					 $pageimg= get_field ('product_image' , $page->ID );
					 $pagetext= get_field ('product_text' , $page->ID );
					 
					 ?>
                     
                     
                   <div class="pro-box"> 
                   	<img src="<?php echo $pageimg; ?>">  
                      <div class="pro-det">
					     <h1><?php echo $page->post_title; ?></h1>
					     <p><?php echo $pagetext; ?></p>
                         <a href="<?php echo get_page_link( $page->ID ); ?>" class="view">VIEW RANGE</a>
                       </div>  
                    </div>            
					 <?php
					 } 
					 
					?>
                   
                 	
              </div>
              
               </div>
                  
          </div>
        </div>
     </section>
<?php get_footer(); ?>



