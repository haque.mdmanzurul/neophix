<?php
/**
 * Template Name: Product single Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header('page'); ?>

<section class="gray-section about-page">
     	<div class="container">
        	<div class="row">
            	<div class="col-lg-12 col-md-12 col-sm-12">
                 	<div class="pro-box-con productbox">
                       <?php if (have_posts()) : while (have_posts()) : the_post();?>
                         	<h2><?php the_title( ); ?></h2>
                             <?php the_content( ); ?>
                 		<?php endwhile; endif; ?>
                       
                 	
              </div>
              
               </div>
                  
          </div>
        </div>
     </section>
<?php get_footer(); ?>



