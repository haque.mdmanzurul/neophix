<?php
/**
 * Template Name: Home Template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
	<section class="gray-section">
     	<div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	  <?php if (have_posts()) : while (have_posts()) : the_post();?>
                             <?php the_content( ); ?>
                 		<?php endwhile; endif; ?>
                        
                        
                        <div class="world-class">
                   
                    
                    <h2><?php the_field ('clienttitle'); ?></h2>
                    <h3><?php the_field ('clientsubtitle'); ?></h3>
                    
                      <ul>
                     <?php $args = array( 'post_type' => 'client-block', 'posts_per_page' => 10 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
                     
                        
                             echo '<li><a href="#">';                            	  
								  the_post_thumbnail(   ); 
                             echo '</a></li>';
						  
                    endwhile; ?>
                    
                    
                   
                    	 
                    </ul>
					</div>
					</div>  
                </div>
            </div>
        </div>
     </section>
<?php get_footer(); ?>