<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('page'); ?>
<section class="gray-section about-page">
     	<div class="container">
        	<div class="row">
             			
            
            	 <div class="page-lt">
                       <?php if (have_posts()) : while (have_posts()) : the_post();?>
                             <?php the_content( ); ?>
                 		<?php endwhile; endif; ?>
                 	
              </div>
                 <div class="page-rt">
                 <?php get_sidebar(); ?>
                 </div>
          </div>
        </div>
     </section>
<?php get_footer(); ?>